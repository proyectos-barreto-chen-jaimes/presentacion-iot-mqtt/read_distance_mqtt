#!/usr/bin/env python3

import paho.mqtt.client as mqtt
import matplotlib.pyplot as plt
import time

# Configuración del broker MQTT y del tópico
broker = "localhost"  # Cambia esto si el broker no está en la misma máquina
port = 1883  # Puerto predeterminado de MQTT
topic = "distance"

# Variables para almacenar los datos
distances = []
timestamps = []

# Crear figura y ejes
fig, ax = plt.subplots()

# Configurar el gráfico
ax.set_xlabel('Tiempo')
ax.set_ylabel('Distancia')
ax.set_title('Gráfico de distancia vs tiempo')
ax.grid(True)

# Inicializar la línea del gráfico
line, = ax.plot(timestamps, distances)

# Función de callback al recibir un mensaje MQTT
def on_message(client, userdata, msg):
    # Obtener la distancia y la marca de tiempo del mensaje
    distance = float(msg.payload)
    timestamp = time.time()

    # Agregar la distancia y la marca de tiempo a las listas
    distances.append(distance)
    timestamps.append(timestamp)

    # Actualizar los datos de la línea del gráfico
    line.set_data(timestamps, distances)

    # Ajustar los límites de los ejes x e y
    ax.relim()
    ax.autoscale_view()

    # Actualizar el gráfico
    fig.canvas.draw()

try:
    # Crear instancia del cliente MQTT
    client = mqtt.Client()

    # Asignar la función de callback al evento on_message
    client.on_message = on_message

    # Conectar al broker MQTT
    client.connect(broker, port)

    # Suscribirse al tópico
    client.subscribe(topic)

    # Mantener la conexión MQTT en segundo plano
    client.loop_start()

    # Mostrar la gráfica vacía antes de iniciar el bucle
    plt.show(block=False)

    # Esperar hasta que se presione Ctrl+C
    while True:
        plt.pause(0.01)

except KeyboardInterrupt:
    # Manejar la excepción de interrupción del teclado (Ctrl+C)
    print('Deteniendo el programa...')
    plt.close()
    client.disconnect()



# #!/usr/bin/env python3

# import paho.mqtt.client as mqtt
# import matplotlib.pyplot as plt
# import time

# # Configuración del broker MQTT y del tópico
# broker = "localhost"  # Cambia esto si el broker no está en la misma máquina
# port = 1883  # Puerto predeterminado de MQTT
# topic = "distance"

# # Variables para almacenar los datos
# distances = []
# timestamps = []

# # Función de callback al recibir un mensaje MQTT
# def on_message(client, userdata, msg):
#     # Obtener la distancia y la marca de tiempo del mensaje
#     distance = float(msg.payload)
#     timestamp = time.time()

#     # Agregar la distancia y la marca de tiempo a las listas
#     distances.append(distance)
#     timestamps.append(timestamp)

# try:
#     # Crear instancia del cliente MQTT
#     client = mqtt.Client()

#     # Asignar la función de callback al evento on_message
#     client.on_message = on_message

#     # Conectar al broker MQTT
#     client.connect(broker, port)

#     # Suscribirse al tópico
#     client.subscribe(topic)

#     # Mantener la conexión MQTT en segundo plano
#     client.loop_start()

#     # Esperar hasta que se presione Ctrl+C
#     while True:
#         # Graficar los datos
#         plt.plot(timestamps, distances)
#         plt.xlabel('Tiempo')
#         plt.ylabel('Distancia')
#         plt.title('Gráfico de distancia vs tiempo')
#         plt.grid(True)
#         plt.show(block=False)
#         plt.pause(0.01)

#         # Pequeño retraso para permitir la correcta visualización del gráfico
#         time.sleep(0.1)

# except KeyboardInterrupt:
#     # Manejar la excepción de interrupción del teclado (Ctrl+C)
#     print('Deteniendo el programa...')
#     plt.close()
#     client.disconnect()






