#!/usr/bin/env python3


# Imports of the necessary libraries
import RPi.GPIO as GPIO
import paho.mqtt.publish as publish
import time

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

# Initialize numeric pins for ultrasound sensors
TRIG = 23
ECHO = 24

# Initialize GPIO for ultrasound sensors
# Ultrasound sensor one
GPIO.setup(TRIG, GPIO.OUT)
GPIO.setup(ECHO, GPIO.IN)

#   Function: readUltrasoundSensor() 
#   Purpose: Read distance from Ultrasound Sensor
#   Argument:
#       trig: Pin of TRIG
#       echo: Pin of ECHO
#   Return:
#       distance in cm
def readUltraSoundSensor(trig, echo):
    # Read ultrasound sensor
    GPIO.output(trig, GPIO.LOW)
    time.sleep(2)
    GPIO.output(trig, GPIO.HIGH)
    time.sleep(0.00001)
    GPIO.output(trig, GPIO.LOW)
    while GPIO.input(echo)==0:
        pulse_start_one = time.time()
    while GPIO.input(echo)==1:
        pulse_end_one = time.time()
    pulse_duration_one = pulse_end_one - pulse_start_one
    return round(pulse_duration_one*17150,2)

# Configuración del broker MQTT
broker = "localhost"  # Cambia esto si el broker no está en la misma máquina
port = 1883  # Puerto predeterminado de MQTT
topic = "distance"


try:
    while True:
        distance = readUltraSoundSensor(TRIG, ECHO)
        publish.single(topic, distance, hostname=broker, port=port)
        print("Distance:", distance, "cm")
        time.sleep(0.1)
except KeyboardInterrupt:
    # Desconectar del broker MQTT
    GPIO.cleanup()
